package towerapi

import grails.rest.Resource

class Cell {

//    String radio
//    Long mcc
//    Long mnc
//    Long lac
//    Long cell
//    String lat
//    String lon
//    Integer rng
//
//
//
//    static mapping = {
//        table 'towers'
//        version false
//        autoTimestamp false
//    }
//    static constraints = {
//        radio maxSize: 255
//        lat maxSize: 20
//        lon maxSize: 20
//    }
    String radio
    Integer mcc
    Integer mnc
    Integer lac
    Integer cell
    Double lat
    Double lon
    Integer rng

    static mapping = {
        table 'cell'
        version false
        autoTimestamp false
        mnc column: "net"
        lac column: "area"
    }
}
