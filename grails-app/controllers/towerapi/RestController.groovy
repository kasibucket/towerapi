package towerapi

import grails.converters.JSON
import groovy.xml.MarkupBuilder



class RestController {


    def getLatLon() {

        def ref = params;
        if (ref.mnc != null) {

            def cell = Cell.find("from Cell as c where mcc = " + ref.mcc
                    + " and mnc = " + ref.mnc
                    + " and lac = " + ref.lac
                    + " and cell = " + ref.cell)
            if (cell != null) {
                ref.remove("controller")
                ref.remove("action")
                ref.putAt("lat", cell.lat)
                ref.putAt("lon", cell.lon)
                ref.putAt("rng", cell.rng)
            }
        } else
            ref = request.JSON
        def cell = Cell.find("from Cell as c where mcc = " + ref.mcc
                + " and mnc = " + ref.mnc
                + " and lac = " + ref.lac
                + " and cell = " + ref.cell)
        if (cell != null) {
            ref.putAt("lat", cell.lat)
            ref.putAt("lon", cell.lon)
            ref.putAt("rng", cell.rng)
        }

        render ref as JSON
    }

    def getLatLonAsXml() {

        def ref = params;
        if (ref.mnc != null) {

            def c = Cell.find("from Cell as c where mcc = " + ref.mcc
                    + " and mnc = " + ref.mnc
                    + " and lac = " + ref.lac
                    + " and cell = " + ref.cell)
            if (c != null) {
                ref.remove("controller")
                ref.remove("action")
                ref.putAt("lat", c.lat)
                ref.putAt("lon", c.lon)
                ref.putAt("rng", c.rng)
            }
        } else
            ref = request.JSON
        def c = Cell.find("from Cell as c where mcc = " + ref.mcc
                + " and mnc = " + ref.mnc
                + " and lac = " + ref.lac
                + " and cell = " + ref.cell)
        if (c != null) {
            ref.putAt("lat", c.lat)
            ref.putAt("lon", c.lon)
            ref.putAt("rng", c.rng)
        }

        def sw = new StringWriter()
        def xml = new MarkupBuilder(sw)
        xml.tower(){
            mcc(ref.mcc)
            mnc(ref.mnc)
            lac(ref.lac)
            cell(ref.cell)
            lat(ref.lat)
            lon(ref.lon)
            rng(ref.rng)
        }
        render "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + sw
    }

    def getLatLonAsTxt() {

        def ref = params;
        if (ref.mnc != null) {

            def cell = Cell.find("from Cell as c where mcc = " + ref.mcc
                    + " and mnc = " + ref.mnc
                    + " and lac = " + ref.lac
                    + " and cell = " + ref.cell)
            if (cell != null) {
                ref.remove("controller")
                ref.remove("action")
                ref.putAt("lat", cell.lat)
                ref.putAt("lon", cell.lon)
                ref.putAt("rng", cell.rng)
            }
        } else
            ref = request.JSON
        def cell = Cell.find("from Cell as c where mcc = " + ref.mcc
                + " and mnc = " + ref.mnc
                + " and lac = " + ref.lac
                + " and cell = " + ref.cell)
        if (cell != null) {
            ref.putAt("lat", cell.lat)
            ref.putAt("lon", cell.lon)
            ref.putAt("rng", cell.rng)
        }

        render "mcc=" + ref.mcc + ";mnc=" + ref.mnc + ";lac=" + ref.lac + ";cell=" + ref.cell + ";lat=" + ref.lat + ";lon=" + ref.lon + ";rng=" + ref.rng
    }
}