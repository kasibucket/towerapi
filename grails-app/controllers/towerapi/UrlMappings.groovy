package towerapi

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }
        //        "/rest/getLatLon/Json" (controller: "Rest", action: "getLatLon", parseRequest: true)
        "/rest/getLatLonAsXml/$mcc?/$mnc?/$lac?/$cell" (controller: "Rest", action: "getLatLonAsXml")
        "/rest/getLatLonAsTxt/$mcc?/$mnc?/$lac?/$cell" (controller: "Rest", action: "getLatLonAsTxt")
        "/rest/getLatLon/$mcc?/$mnc?/$lac?/$cell" (controller: "Rest", action: "getLatLon")
        "/"(controller: 'application', action:'index')
        "500"(view: '/error')
        "404"(view: '/notFound')
    }
}