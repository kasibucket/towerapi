package towerapi

import grails.core.GrailsApplication
import grails.util.Environment
import grails.plugins.*

class ApplicationController implements PluginManagerAware {

    GrailsApplication grailsApplication
    GrailsPluginManager pluginManager

    def index() {
        render "TowerAPI is online"
        //[grailsApplication: grailsApplication, pluginManager: pluginManager]
    }
}
