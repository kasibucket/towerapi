package towerapi


import grails.test.mixin.integration.Integration
import grails.transaction.*
import static grails.web.http.HttpHeaders.*
import static org.springframework.http.HttpStatus.*
import spock.lang.*
import geb.spock.*
import grails.plugins.rest.client.RestBuilder

@Integration
@Rollback
class RestControllerSpec extends GebSpec {
    RestBuilder restBuilder() {
        new RestBuilder()
    }

    def setup() {
    }

    def cleanup() {
    }

    void "Test the homepage"() {
        when:"The home page is requested"
            def resp = restBuilder().get("$baseUrl/")

        then:"The response is correct"
            resp.status == OK.value()
            resp.headers[CONTENT_TYPE] == ['text/html;charset=utf-8']
            resp.body == 'TowerAPI is online'
    }

    void "Test JSON response from JSON Post"() {
        when:"JSON: {\"mnc\":\"03\",\"mcc\":\"262\",\"cell\":\"137209857\",\"lac\":\"40093\"} is posted"
        def resp = restBuilder().post("$baseUrl/rest/getLatLon/") {
            json {
                mnc = "03"
                mcc = "262"
                cell = "137209857"
                lac = "40093"
            }
        }

        then:"The response is correct"
        resp.status == OK.value()
        resp.headers[CONTENT_TYPE] == ['application/json;charset=UTF-8']
        resp.json.mnc == "03"
        resp.json.mcc == "262"
        resp.json.cell == "137209857"
        resp.json.lac == "40093"
        resp.json.lat == 52.462458
        resp.json.lon == 13.32911
        resp.json.rng == 355
    }

    void "Test JSON response when no Cell/LatLon found from JSON Post"() {
        when:"JSON: {\"mnc\":\"03\",\"mcc\":\"262999\",\"cell\":\"137209857\",\"lac\":\"40093\"} is posted"
        def resp = restBuilder().post("$baseUrl/rest/getLatLon/") {
            json {
                mnc = "03"
                mcc = "262999"
                cell = "137209857"
                lac = "40093"
            }
        }

        then:"The response is correct"
        resp.status == OK.value()
        resp.headers[CONTENT_TYPE] == ['application/json;charset=UTF-8']
        resp.json.mnc == "03"
        resp.json.mcc == "262999"
        resp.json.cell == "137209857"
        resp.json.lac == "40093"
        resp.json.lat == null
        resp.json.lon == null
        resp.json.rng == null
    }

    void "Test JSON response from URL Get"() {
        when:"$baseUrl/rest/getLatLon/262/03/40093/137209857 is called"
        def resp = restBuilder().get("$baseUrl/rest/getLatLon/262/03/40093/137209857")

        then:"The response is correct"
        resp.status == OK.value()
        resp.headers[CONTENT_TYPE] == ['application/json;charset=UTF-8']
        resp.json.mnc == "03"
        resp.json.mcc == "262"
        resp.json.cell == "137209857"
        resp.json.lac == "40093"
        resp.json.lat == 52.462458
        resp.json.lon == 13.32911
        resp.json.rng == 355

    }

    void "Test JSON response when no Cell/LatLon found from URL Get"() {
        when:"$baseUrl/rest/getLatLon/262999/03/40093/137209857 is called"
        def resp = restBuilder().get("$baseUrl/rest/getLatLon/262999/03/40093/137209857")

        then:"The response is correct"
        resp.status == OK.value()
        resp.headers[CONTENT_TYPE] == ['application/json;charset=UTF-8']
        resp.json.mnc == "03"
        resp.json.mcc == "262999"
        resp.json.cell == "137209857"
        resp.json.lac == "40093"
        resp.json.lat == null
        resp.json.lon == null
        resp.json.rng == null

    }

}
